#Notificação de mudanças


Nós preparamos essa página especialmente para que você possa ficar por dentro de todas as mudaças que tiver sendo feitas enquanto construimos nossa API.


As modificaçõe estão listadas abaixo ordenadas da mais recente às mais antigas.



#CHANGELOG


##JANEIRO/2016


###Atualizações em 15/01/2016


**[Nova API]** incluído documento de exemplo para definir a estrutura de documentação de cada API, veja [aqui](exemplo-documentacao.md)
