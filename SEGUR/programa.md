#Programa


##**Resumo** 

Manutenção de Programas disponíveis para acesso no sistema.
Esta API permite pela inclusão, alteração, exclusão física e consulta de dados de Programas.

##Atributos da API
Nome            |Descrição                                |Tipo de Dado  |Obrig.|Tabela.Campo              
----------------|-----------------------------------------|--------------|------|--------------------------
codigoPrograma  | Código Único do Programa                |nvarchar(50)  |Sim   |Programa.chCodPrograma    
tituloPrograma  | Título do Programa exibido no  menu     |nvarchar(80)  |Sim   |Programa.chDesTitPrograma 
observacao      | Descrição detalhada do programa         |text          |Não   |Programa.chDesObservacao  
caminhoPrograma | Caminho(pasta) relativo do programa     |nvarchar(100) |Não   |Programa.chDesPasta       
disponivelMenu  | Identifica Programa Disponivel no menu  |bit           |Sim   |Programa.lgDisponivelMenu 

##**Regras de negócio**

- Regras para Inclusão:
   - Atributo codigoPrograma:
      - Será informado somente na inclusão
      - Deve ser informado (Diferente de Null e Vazio)
      - O código não poder ser duplicado;
- Regras para Alteração:
- Regras para (Inclusão/Alteração):
  - Atributo tituloPrograma:
      - Deve ser informado
   - Atributo observacao:
      - Deve ser diferente de Null, mas pode ser vazio
   - Atributo caminhoPrograma:
      - Deve ser diferente de Null, mas pode ser vazio
- Regras para Exclusão:    

---

##Criar Programa ```POST```

Por meio deste método da API é possível criar um Programa.

###ENDPOINT

**POST** ```.../segur/api/v1/programa```

###REQUEST

**Content-Type:** ```application/json```

Authorization: ```token do usuário```

JSON
```
{
    "codigoPrograma": "cadPrograma001",
    "tituloPrograma": "Cadastro de Programas",
    "observacao":     "Manutenção de programas disponiveis no sistema",
    "caminhoPrograma": "segur/api/",
    "disponivelMenu":  "1"
    
}
```

###RESPONSE


201 (Created)

**Content-Type:** ```application/json```
JSON
```
{
    "codigoPrograma": "cadPrograma001",
    "tituloPrograma": "Cadastro de Programas",
    "observacao":     "Manutenção de programas disponiveis no sistema",
    "caminhoPrograma": "segur/api/",
    "disponivelMenu":  "1"
}
```

-------------------------

##Alterar Programa ```PUT```

Por meio deste método da API é possível alterar dados de um Programa.

###ENDPOINT

**PUT** ```.../segur/api/v1/programa```

###REQUEST

**Content-Type:** ```application/json```

Authorization: ```token do usuário```

JSON
```
{
    "codigoPrograma": "cadPrograma001",
    "tituloPrograma": "Cadastro de Programas",
    "observacao":     "Manutenção de programas disponiveis no sistema",
    "caminhoPrograma": "segur/api/",
    "disponivelMenu":  "1"
}
```

###RESPONSE

201 (Modified)

**Content-Type:** ```application/json```
JSON
```
{
    "tituloPrograma": "Cadastro de Programas",
    "observacao":     "Manutenção de programas disponiveis no sistema",
    "caminhoPrograma": "segur/api/",
    "disponivelMenu":  "1"
}
```

-------------------------

##Excluir Programa ```DELETE```

Por meio deste método da API é possível excluir um Programa.

###ENDPOINT

**DELETE** ```.../segur/api/v1/programa```

###REQUEST

**Content-Type:** ```application/json```

Authorization: ```token do usuário```

JSON
```
{
    "codigoPrograma": "cadPrograma001"
}
```

###RESPONSE

201 (Deleted)





