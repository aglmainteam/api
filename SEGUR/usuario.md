#Usuario

##**Resumo** 

Representa os usuários de acesso ao sistema, utilizada no módulo de Segurança.

Esta API permite a criação, alteração, consulta detalhada, recuperação de senha e troca de senha dos usuários de acesso ao sistema.

##**Regras de negócio**

*Escreva nesse espaço quando necessário as regras de negócio associadas a esta API*


##Atributos
Nome                |Descrição                                |Tipo de Dado  |Obrigatório
--------------------|-----------------------------------------|--------------|-----------
id                  |id autonumeração da pessoa               |bigint        |Não        
nome                |Nome do usuário                          |varchar(90)   |Sim        
                    |                                         |              |

##Criar Usuário ```POST```


Por meio desta API é possível criar um Usuário


###ENDPOINT


**POST** ```.../segur/api/v001/usuario```


###REQUEST

**Content-Type:** ```application/json```

Authorization: ```token do usuário```

JSON
```
{
    "nome": "nome do aluno",
    "email": "email do aluno"
}
```

###RESPONSE


201 (Created)

**Content-Type:** ```application/json```
JSON
```
{
    "id": numero gerado
    "nome": "nome do aluno",
    "email": "email do aluno"
}
```


Continue o documento especificando as outras interfaces pra Aluno, como por exemplo: Alterar aluno, Selecionar Alunos, etc.
