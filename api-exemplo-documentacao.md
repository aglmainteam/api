#Nome da API, exemplo: Alunos


##**Resumo** 

*Escreva aqui breve descrição do propósito da API, veja exemplo abaixo:*

representa o cliente do seu negócio, esta API permite a criação, alteração e consulta de alunos.


##**Regras de negócio**

*Escreva nesse espaço quando necessário as regras de negócio associadas a esta API*


##Atributos (estrutura de dados da tabela)
Nome                |Descrição                                |Detalhes
--------------------|-----------------------------------------|-------------------------
id                  |id autonumeração do aluno                |bigint
nome                |Nome do aluno                            |string(90), obrigatório
email               |Email do aluno                           |string(90), obrigatório


**Atenção!** *o exemplo abaixo representa apenas um dos métodos da API, que no caso é de inserção, outros métodos deverão ser descritos nesse documento repetindo a mesma estrutura*


##Criar aluno ```POST```


Por meio desta API é possível criar um Aluno.


###ENDPOINT


**POST** ```https://meusite.com/teste/api/{versão da api}/alunos```


###REQUEST

**Content-Type:** ```application/json```

Authorization: ```token do usuário```

JSON
```
{
    "nome": "nome do aluno",
    "email": "email do aluno"
}
```

###RESPONSE


201 (Created)

**Content-Type:** ```application/json```
JSON
```
{
    "id": numero gerado
    "nome": "nome do aluno",
    "email": "email do aluno"
}
```


Continue o documento especificando as outras interfaces pra Aluno, como por exemplo: Alterar aluno, Selecionar Alunos, etc.
