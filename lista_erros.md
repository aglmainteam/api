#Lista de erros


###ATRIBUTOS
Nome	    |Descrição	                    |Detalhes
------------|-------------------------------|--------
code	    |Código identificador do erro	|string
path	    |Parâmetro relacionado ao erro	|string
description	|Descrição do erro              |string


Todos os erros mapeados retornarão com o http status 400 para sua aplicação. Caso você receba um http status 500 para uma requisição válida por gentileza avise a equipe.


###EXEMPLO
```javascript
{
  "errors": [
    {
      "code": "PASS-001",
      "path": "acessoNegado",
      "description": "Seu usuário não possui privilégio de acesso a essa funcionalidade"
    }
  ]
}
```

Abaixo estão listados os erros em cada uma das APIs separados pelo titulo de cada API



##Erros **Alunos**

Code        |Path                |Description
```
aln-001     |acessoNegado        |Seu usuário não possui privilégio de acesso a essa funcionalidade
```
```
aln-002     |aluno.email        |email inválido
```
```
aln-003     |aluno.email        |email obrigatório
```
```
aln-004     |aluno.email        |email já existente
```



##Erros **Outra API**

Escreva abaixo a descrição de erros de outra API